import React from 'react';
import { Consumer } from '../../containers/SharedContext/SharedContext';
import BookCard from '../BookCard/BookCard';

const BookList = _ => (
    <Consumer>
        {( { books, filteredBooks, loading } ) => {
            if(loading) {
                return <div className="loading">LOADING...</div>;
            }

            return <div className="BookList">
                {filteredBooks.length > 0 ?
                    filteredBooks.map(item => <BookCard key={item.id} book={item} />)
                 :
                    books.map(item => <BookCard key={item.id} book={item} />)
                }
                <div className="BookCard hidden">
                    <div className="BookCardImage"></div>
                </div>
                <div className="BookCard hidden">
                    <div className="BookCardImage"></div>
                </div>
            </div>;
        }}
    </Consumer>
)
 
export default BookList;