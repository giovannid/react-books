import React from 'react'
import { Consumer } from '../../containers/SharedContext/SharedContext';
import Nav from '../Nav/Nav';

const SearchBar = _ => {
    return (
        <Consumer>
            {({ filterBooks }) => (
                <div className="SearchBar">
                    <Nav className="SearchBarNav" />
                    <input type="text" placeholder="Search for a book, author, ISBN..." onChange={(event) => filterBooks(event.target.value)} />
                </div>
            )}
        </Consumer>
    );
}
 
export default SearchBar;