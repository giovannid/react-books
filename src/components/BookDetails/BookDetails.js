import React from 'react';
import { Link } from 'react-router-dom'
import { Consumer } from '../../containers/SharedContext/SharedContext';
import NoMatch from '../NoMatch/NoMatch';

const BookDetails = ({ match: { params: { slug } }}) => (
    <Consumer>
        {({ getBookBySlug, loading }) => {
            if(loading) {
                return <div className="loading">LOADING...</div>;
            }
            const book = getBookBySlug(slug);

            if(book) {
                return (
                    <div className="BookDetails">
                        <div className="BookImage">
                            <img src={book.coverImg} alt={book.title} width={300} />
                        </div>
                        <div className="BookInfo">
                            <h1>{book.title}</h1>
                            <h3>by {book.author.map((item, index, arr) => (
                                <Link key={index} to={`/author/${item.slug}`}>
                                    {arr.length-1 !== index ? `${item.name}, ` : `${item.name}`}
                                </Link>
                            ))}</h3>

                            <div className="BookDesc">
                                <p>Nulla luctus egestas tellus a tincidunt. Nulla vitae magna non elit consequat tempor. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc facilisis, libero vitae varius dignissim, tellus dolor tristique orci, non cursus nunc sapien ut ipsum. Donec massa sapien, tempus non maximus ut, viverra consectetur purus. Sed rhoncus quam non enim pretium, ac dapibus ipsum aliquet. Praesent sit amet efficitur elit.</p>
                            </div>
                            <span className="BookPageN"><strong>Pages:</strong> {book.pages}</span>
                            <span className="BookISBN"><strong>ISBN:</strong> {book.ISBN}</span>
                            <span className="BookLang"><strong>Language: </strong> {book.lang}</span>
                        </div>
                    </div>
                );
            } else {
                return <NoMatch />;
            }
        }}
    </Consumer>
)
 
export default BookDetails;