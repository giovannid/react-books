import React from 'react';
import { Link } from 'react-router-dom'

const BookCard = ({ book }) => {
    return (
        <div className="BookCard">
            <div className="BookCardImage">
                <Link to={`/book/${book.slug}`}>
                    <img src={book.coverImg} alt={book.title} />
                </Link>
            </div>
            <div className="BookCardInfo">
                <span className="title">
                    <Link title={book.title} to={`/book/${book.slug}`}>{(book.title.length > 19) ? `${book.title.slice(0, 18)}...` : book.title }</Link>
                </span>
                <span className="author">
                        {book.author.map((item, index, arr) => (
                            <Link key={index} to={`/author/${item.slug}`}>
                                {arr.length-1 !== index ? `${item.name},` : `${item.name}`}
                            </Link>
                        ))}
                </span>
            </div>
        </div>
    );
}
 
export default BookCard;