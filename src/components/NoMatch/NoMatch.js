import React from 'react'
import { Link } from 'react-router-dom'

const NoMatch = _ => {
    return ( 
        <div className="NoMatch">
            <h1>Not Found!</h1>
            <Link to="/">Go Back</Link>
        </div>
     );
}
 
export default NoMatch;