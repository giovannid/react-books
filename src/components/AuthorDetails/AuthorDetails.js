import React from 'react'
import { Consumer } from '../../containers/SharedContext/SharedContext';
import NoMatch from '../NoMatch/NoMatch';
import BookCard from '../BookCard/BookCard';

const AuthorDetails = ({ match: { params: { slug } }}) => (
    <Consumer>
        {({ getBooksByAuthor, loading }) => {
            if(loading) {
                return <div className="loading">LOADING...</div>;
            }

            const findBooks = getBooksByAuthor(slug);

            if(findBooks) {
                return <div className="AuthorDetails">
                    <h1>{findBooks.author.name}</h1>

                    <div className="BookList">
                        {findBooks.books.map(book => <BookCard key={book.id} book={book} />)}
                        <div className="BookCard hidden">
                            <div className="BookCardImage"></div>
                        </div>
                        <div className="BookCard hidden">
                            <div className="BookCardImage"></div>
                        </div>
                    </div>
                </div>
            } else {
                return <NoMatch />;
            }
        }}
    </Consumer>
)
 
export default AuthorDetails;