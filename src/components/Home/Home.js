import React from 'react'
import BookList from '../BookList/BookList';

const Home = _ => {
    return (
        <React.Fragment>
            <BookList />
        </React.Fragment>
    );
}

export default Home;