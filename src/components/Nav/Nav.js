import React from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as Icon } from '../../assets/images/home.svg';

const Nav = (props) => {
    return (
        <nav {...props}>
            <Link to="/">
                <Icon />
            </Link>
        </nav>
    );
}
 
export default Nav;