import React from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom'
import { Provider } from '../../containers/SharedContext/SharedContext';

import SearchBar from '../SearchBar/SearchBar';
import Home from '../Home/Home';
import BookDetails from '../BookDetails/BookDetails';
import AuthorDetails from '../AuthorDetails/AuthorDetails';
import NoMatch from '../NoMatch/NoMatch';
import Nav from '../Nav/Nav';

import './App.scss';

export default function App() {
  return (
    <Provider>
        <Router>
            <div className="App">
                <Nav className="MainNav" />
                <SearchBar />
                <Switch>
                    <Route path="/" exact component={Home} />
                    <Route path="/book/:slug" component={BookDetails} />
                    <Route path="/author/:slug" component={AuthorDetails} />
                    <Route component={NoMatch} />
                </Switch>
            </div>
        </Router>
    </Provider>
  );
}