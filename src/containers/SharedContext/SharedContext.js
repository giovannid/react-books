import React, { Component } from 'react';
import { getBooks } from '../../api/api';
import { find as _find, filter as _filter } from 'lodash';

const ContextProvider = React.createContext();

export class Provider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            booksById: [],
            entities: {},
            filteredBooks: [],
            loading: true
        }
    }

    componentDidMount() {
        getBooks().then(res => {
            const { entities, result } = res;
            this.setState({
                entities,
                booksById: result,
                loading: false
            });
        })
    }

    filterBooks = arg => {
        let filter = this.state.booksById.filter(id => {
            let book = this.getBook(id);

            let argLower = arg.toLowerCase().trim();
            let author   = this.getAuthors(book.author).map(author_obj => author_obj.name.toLowerCase()).toString();
            let title    = book.title.toLowerCase();
            let isbn     = book.ISBN;

            if(argLower.length > 0) {
                if((title.indexOf(argLower) !== -1) || (author.indexOf(argLower) !== -1) || (isbn.indexOf(argLower) !== -1)) {
                    return true;
                }
            }
            return false;
        });

        this.setState({
            filteredBooks: filter
        });
    }

    getBook = id => this.state.entities.books[id];

    getBookBySlug = slug => {
        const { entities: { books = {} } } = this.state;

        let findBook = _find(books, { 'slug': slug });
        if(findBook !== undefined) {
            let author = this.getAuthors(findBook.author);
            return Object.assign({}, findBook, { author });
        }
        return false;
    }
    
    getBooks = ids => ids.map(id => {
        let book = this.getBook(id);
        let author = this.getAuthors(book.author);
        return Object.assign({}, book, { author });
    });

    getBooksByAuthor = author => {
        const findAuthor = this.getAuthorBySlug(author);
        const books = this.getBooks(this.state.booksById);
        if(findAuthor) {
            const findBooks = _filter(books, { author: [ { slug: author } ] });
            return {
                books: findBooks,
                author: findAuthor
            }
        }

        return false;
    }

    getAuthor = id => this.state.entities.authors[id];
    
    getAuthorBySlug = slug => {
        const { entities: { authors = {} } } = this.state;

        let findAuthor = _find(authors, { 'slug': slug });
        if(findAuthor !== undefined) {
            return findAuthor;
        }
        return false;
    };
    
    getAuthors = ids => ids.map(id => this.getAuthor(id));

    render() {
        const { children } = this.props;
        const { filteredBooks, booksById, loading } = this.state;

        let parsedBooksById = this.getBooks(booksById);
        let parsedFilteredBooks = this.getBooks(filteredBooks);

        return (
            <ContextProvider.Provider value={{
                loading,
                books: parsedBooksById, 
                filteredBooks: parsedFilteredBooks, 
                filterBooks: this.filterBooks,
                getBookBySlug: this.getBookBySlug,
                getBooksByAuthor: this.getBooksByAuthor
            }}>
                {children}
            </ContextProvider.Provider>
        );
    }    
}

export const Consumer = ContextProvider.Consumer;