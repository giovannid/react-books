import { schema } from 'normalizr';

const author = new schema.Entity("authors");
const book = new schema.Entity("books", {
  author: [author]
});

export const arrbooks = new schema.Array(book);