// import axios from 'axios';
import { normalize } from 'normalizr';
import { arrbooks } from '../api/schema';

// const requestWrapper = args => {
//     let { url, data = null, method = "get" } = args;
//     return axios({
//         method: method,
//         url: url,
//         data: data
//     });
// }



let content = `{
    "code": 200,
    "response":  [
   {
     "id": "001",
     "title": "Lobortis vel gravida vitae",
     "slug": "lobortis-vel-gravida-vitae",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "001",
         "name": "Cursus Sed",
         "slug": "cursus-sed"
       },
       {
         "id": "002",
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
       }
     ],
     "pages": 437,
     "ISBN": "0062795252",
     "lang": "English"
   },
   {
     "id": "002",
     "title": "Eu pretium nisi",
     "slug": "eu-pretium-nisi",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
     {
         "id": "002",
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
     }
     ],
     "pages": 373,
     "ISBN": "0062457799",
     "lang": "English"
   },
   {
     "id": "003",
     "title": "Integer a elementum mauris",
     "slug": "integer-a-elementum-mauris",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "003",
         "name": "Venenatis Suscipit",
         "slug": "venenatis-suscipit"
       }
     ],
     "pages": 320,
     "ISBN": "0545509890",
     "lang": "English"
   },
   {
     "id": "004",
     "title": "Turpis Finibus",
     "slug": "turpis-finibus",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "003",  
         "name": "Venenatis Suscipit",
         "slug": "venenatis-suscipit"
       }
     ],
     "pages": 330,
     "ISBN": "0545858267",
     "lang": "English"
   },
   {
     "id": "005",
     "title": "Lorem convallis Quis",
     "slug": "lorem-convallis-quis",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "002",
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
       }
     ],
     "pages": 320,
     "ISBN": "1616956933",
     "lang": "English"
   },
   {
     "id": "006",
     "title": "Lacinia et vestibulum a",
     "slug": "lacinia-et-vestibulum-a",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "001",
         "name": "Cursus Sed",
         "slug": "cursus-sed"
       },
       {
         "id": "002",
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
       }
     ],
     "pages": 437,
     "ISBN": "0062795252",
     "lang": "English"
   },
   {
     "id": "007",
     "title": "Maecenas ac justo",
     "slug": "maecenas-ac-justo",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "002",  
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
       }
     ],
     "pages": 373,
     "ISBN": "0062457799",
     "lang": "English"
   },
   {
     "id": "008",
     "title": "Maecenas ac justo ligula",
     "slug": "maecenas-ac-justo-ligula",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "003",  
         "name": "Venenatis Suscipit",
         "slug": "venenatis-suscipit"
       }
     ],
     "pages": 320,
     "ISBN": "0545509890",
     "lang": "English"
   },
   {
     "id": "009",
     "title": "Pellentesque non porttitor odio",
     "slug": "pellentesque-non-porttitor-odio",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "003",  
         "name": "Venenatis Suscipit",
         "slug": "venenatis-suscipit"
       }
     ],
     "pages": 330,
     "ISBN": "0545858267",
     "lang": "English"
   },
   {
     "id": "010",
     "title": "Donec sollicitudin condimentum",
     "slug": "Donec-sollicitudin-condimentum",
     "coverImg": "https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1474171184i/136251._SY475_.jpg",
     "author": [
       {
         "id": "002",
         "name": "Lectus Mattis",
         "slug": "lectus-mattis"
       }
     ],
     "pages": 320,
     "ISBN": "1616956933",
     "lang": "English"
   }
 ]
 }`;

 const requestWrapper = _ => {
    return new Promise(resolve => {
        setTimeout(() => resolve(content), 500);
    });
}

export const getBooks = _ => {
    return new Promise(resolve => {
        // requestWrapper({ url: 'http://www.mocky.io/v2/5d18d7ef300000f93c8beb90' })
        // .then(res => resolve(normalize(res.data.response, arrbooks)))
        // .catch(_ => resolve({ response: "Error requesting list of books!" }));
        requestWrapper().then(res => {
            let parsed = JSON.parse(res);
            resolve(normalize(parsed.response, arrbooks))
        })
    });
};